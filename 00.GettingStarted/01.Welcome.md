# Welcome to the CTS!

### Workstation and organization users

In order to start the journey you need to do the following:

1. Login to [Click](https://clickportal.idf.il/) with your ID.
2. Login to Kitbag and ask to sign on the PC/Laptop you received from you hofef

### Permissions

Ask your hofef to add your fingerprint to all the doors in the building

1. Core's black door
2. Big Data - Platforms yellow door
3. CJ white door

### Whatsapp groups

Ask your hofef to add you to the CTS whatsapp community

### General
1. Go to the Kirya's medical center and open a request to reassign you to them

# Networks

### Introduction
- Networking introduction

### Layer 3
- IP addresses
- Subnets
    - Class subnets
    - CIDR subnets
    - Reserved network addresses
- Static routes
- Protocols
    - ARP
    - ICMP
    - NAT
- Routing schemes
    - Unicast
    - Broadcast
    - Multicast
    - Anycast
    - Geocast
- Packets
- Routers

### Layer 4
- TCP
- UDP

### HTTP
- HTTP

### DHCP
- DHCP flow
- DHCP server
- `dhclient`
  
### DNS
- /etc/hosts
- DNS server
- /etc/resolv.conf
- `dig`
- `host`

# Virtualization


### Hypervisors
- Types of hypervisors

### Containers
- namespaces
- cgroups
- Linux containers
- Docker
  - Docker volumes type
  - Docker networks
  - Docker images
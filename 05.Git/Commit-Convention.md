# Portal™ Commit message convention

## Summary

In the Portal™ we are using commit convention for each message that is being written, We will make sure that our commits are small as possible but still makes sense to look at and read.

## Rules

Each message will begin with a prefix representing the type of the commit:

- `fix`: Fix a bug or an issue
- `feat`: Introduce new feature
- `chore`: Change of a file that is not part of the code, Docs, Assets, Config etc...
- `hotfix`: Quick urgent fix of a bug
- `refactor`: Refactor of a function, component or a file
- `del`: Delete bug chunk of code, usually dead code

The next part will represnt the changed files or parts during the commit. It will be written in a brackets.

- (Most Common) One changed file: `(file.ext)`
- Two changed files: `(button.tsx, button.service.ts)`
- Two or more files changed in the same directory with the same name but different extensions: `(file)`
- Several files changed with the same context, usually a refactor for some function or a class (Not more then 5 files) `(several)`
- (Not Recommended) More then 5 files chnged and must be commited in one single commit `(many)`

Finally we will write quick explenation of what have been changed. We will make sure that the messages written in present simple and not in past.

We will try not to add the word fix or delete to the commit message because it is already in the prefix of the commit.

The message will not be longer then 7~ words

Examples:
- Implemented sum function -> Implement sum function
- Removed devision by zero bug -> Remove Division by zero bug
- Added new asset file -> Add new asset file

## Examples

```
fix(index.tsx): font bug
```

```
feat(Navbar.tsx): Add navbar search field
```

```
refactor(button): Redesign main button
```

```
hotfix(service.py): scopes assignment
```

```
chore(README.md): Add new section to docs
```

```
del(several): Unused svg files
```
